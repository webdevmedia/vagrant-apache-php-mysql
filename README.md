# README #

This is creating a [Vagrant Box](https://www.vagrantup.com/) based on [Scotch Box](https://box.scotch.io/)

### Default Setup ###
* **IP:** 172.16.0.2
* **Host:** your-project.wp,
* **Alias:** www.your-project.wp,
* **Forwarded ports:**
     * "80": "80",
     * "9200": "9200"